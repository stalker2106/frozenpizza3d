#ifndef INVENTORY_HH_
#define INVENTORY_HH_

#include <Ogre.h>
#include <OIS.h>
#include "iddb.h"

class Inventory
{
public:
	Inventory();
	~Inventory();

	bool update(OIS::Keyboard *keyboard);

private:
};

#endif /* !INVENTORY_HH_ */