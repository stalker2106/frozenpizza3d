#ifndef HUD_HH_
#define HUD_HH_

#include <Ogre.h>

class HUD
{
public:
	HUD(Ogre::Root *root);
	~HUD();

	void updateHealth(float health, float healthMax);

private:
	Ogre::Image *_crosshair;
	Ogre::Image *_healthbar, *_healthstate;
};

#endif /* !HUD_HH_ */