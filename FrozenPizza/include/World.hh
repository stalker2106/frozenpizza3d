#ifndef WORLD_HH_
#define WORLD_HH_

#include <Ogre.h>

class World
{
public:
	World::World(Ogre::Root *root, Ogre::SceneManager *smgr);
	World::~World();
	
private:
	//irr::scene::ISceneNode *_skybox;
	//irr::scene::ITerrainSceneNode *_terrain;
};

#endif /* !WORLD_HH_*/