#ifndef ITEM_HH_
#define ITEM_HH_

#include <Ogre.h>
#include <pugixml.hpp>
#include <fstream>
#include <string>

class Item
{
public:
	Item(const std::string &id) : _id(id) {};
	virtual ~Item() {};

	std::string &getId() { return (_id); };
	Ogre::Entity *getModel() { return (_model); };
	
protected:
	std::string _id;
	Ogre::Entity *_model;
};

#endif /* !ITEM_HH_ */