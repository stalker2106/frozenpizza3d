/*        *           *           *         */
/*  File: iddb.h                            */
/*  Author: Maxime MARTENS                  */
/*  Brief: This file gather all IDs used in */
/*  FrozenPizza 3D                          */
/*                                          */
/*        *           *           *         */

#ifndef IDDB_H_
#define IDDB_H_

#define IDC_PLAYER_FPSCAMERA 1
#define IDP_INVENTORY_FRAME 2

#endif /* !IDDB_H_ */