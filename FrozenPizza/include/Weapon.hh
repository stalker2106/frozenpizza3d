#ifndef WEAPON_HH_
#define WEAPON_HH_

#include <Ogre.h>
#include <pugixml.hpp>
#include "Item.hh"

#define WEAPON_XMLDATA_PATH(x) (std::string("./Data/resources/weapons/"+x+".xml").c_str())
#define WEAPON_MODEL_PATH(x) (std::string("./Data/models/weapons/"+x+".obj").c_str())
#define WEAPON_TEXTURE_PATH(x) (std::string("./Data/textures/weapons/"+x+".jpg").c_str())


bool atob(const pugi::char_t *str);

typedef struct s_firearmData
{
	Ogre::int32 magSize;
	Ogre::int32 range;
} t_firearmData;

typedef struct s_weaponData
{
	Ogre::int32 damage;
	float cooldown;
	bool twoHanded;
	t_firearmData *fad;
} t_weaponData;

class Weapon : public Item
{
public:
	Weapon(const std::string &id);
	~Weapon();
	
	static t_weaponData *getWeaponDataXML(const std::string &weaponId);

private:
	t_weaponData *_weaponData;
};

#endif /* !WEAPON_HH_ */