#ifndef ENGINE_HH_
#define ENGINE_HH_

#include <Ogre.h>
#include <OIS.h>
#include <functional>
#include "iddb.h"
#include "World.hh"
#include "Player.hh"

enum GameState {
	Playing = 0,
	Menu
};

class Engine : public Ogre::FrameListener
{
public:
	Engine(unsigned int width, unsigned int height);
	~Engine();
	
	bool init();
	void initOIS();
	void drop();
	void toggleMouseHook();
	void tutorial();

	bool frameStarted(const Ogre::FrameEvent &e);
	bool frameEnded(const Ogre::FrameEvent &e);

	int run();

	static Engine *instance; //Public hook on engine instance

private:	
	float _time;
	bool _run;
	GameState _gameState;
	Ogre::Log* _log;
	Ogre::RenderWindow *_window;
	OIS::InputManager *_imgr;
	OIS::Mouse *_mouse;
	OIS::Keyboard *_keyboard;
	Ogre::Root *_root;
	Ogre::SceneManager *_smgr;
	Player *_player;
	World *_world;
};

#endif /* !ENGINE_HH_ */