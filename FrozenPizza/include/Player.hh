#ifndef PLAYER_HH_
#define PLAYER_HH_

#include <Ogre.h>
#include <OIS.h>
#include "Inventory.hh"
#include "Weapon.hh"
#include "HUD.hh"

#define DEFAULT_PLAYER_HEIGHT 60
#define DEFAULT_FPSCAMERA_HEIGHT 50
#define DEFAULT_PLAYER_WIDTH 20
#define DEFAULT_PLAYER_GRAVITY -1000

typedef struct s_cameraNodes
{
	Ogre::SceneNode *base;
	Ogre::SceneNode *yaw;
	Ogre::SceneNode *pitch;
	Ogre::SceneNode *roll;
} t_cameraNodes;

typedef struct s_stat
{
	Ogre::int32 current;
	Ogre::int32 max;
	struct s_stat(Ogre::int32 c, Ogre::int32 m)
	{
		current = c;
		max = m;
	}
} t_stat;

class Player
{
public:
	Player(Ogre::Root *root, Ogre::SceneManager *smgr);
	~Player();
	
	bool toggleMouseHook();
	void dropHands(Ogre::Root *root, Ogre::SceneManager *smgr);

	bool update(OIS::Keyboard *keyboard, Ogre::Root *root, Ogre::SceneManager *smgr);

	bool takeDamage(Ogre::int32 damage);
	
protected:
	HUD *_hud;
	Ogre::Viewport *_viewport;
	Ogre::Camera *_camera;
	t_cameraNodes *_cameraNodes;
	Ogre::SceneNode *_viewmodel;
	Inventory *_inventory;
	Item *_hands;
	t_stat _hp;
	t_stat _hunger;
	t_stat _thirst;
};

#endif /* PLAYER_HH_ */