#include "Player.hh"

Player::Player(Ogre::Root *root, Ogre::SceneManager *smgr)
	: _hp(100, 100), _hunger(100, 100), _thirst(100, 100)
{
	_inventory = new Inventory();
	_hands = new Weapon("ak47");
	_hud = new HUD(root);
	_camera = new Ogre::Camera("FPS", smgr);
	_cameraNodes = new t_cameraNodes();
	_cameraNodes->base = smgr->getRootSceneNode()->createChildSceneNode();
	_cameraNodes->yaw = _cameraNodes->base->createChildSceneNode();
	_cameraNodes->pitch = _cameraNodes->yaw->createChildSceneNode();
	_cameraNodes->roll = _cameraNodes->pitch->createChildSceneNode();
	_cameraNodes->roll->attachObject(_camera);
}

Player::~Player()
{
	delete (_inventory);
	delete (_hud);
	delete (_cameraNodes);
}

bool Player::toggleMouseHook()
{
	bool active = true;

	return (active);
}

void Player::dropHands(Ogre::Root *root, Ogre::SceneManager *smgr)
{
	if (_hands)
	{
		_hands = NULL;
	}
}

bool Player::update(OIS::Keyboard *keyboard, Ogre::Root *root, Ogre::SceneManager *smgr)
{
	_inventory->update(keyboard);
	if (keyboard->isKeyDown(OIS::KC_G))
		dropHands(root, smgr);
	return (true);
}

bool Player::takeDamage(Ogre::int32 damage)
{
	_hp.current -= damage;
	if (_hp.current <= 0)
		_hp.current = 0;
	_hud->updateHealth(_hp.current, _hp.max);
	return (_hp.current == 0 ? false : true);
}