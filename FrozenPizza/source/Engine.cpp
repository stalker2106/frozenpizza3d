#include "Engine.hh"

Engine *Engine::instance = NULL; //public hook on Engine instance

Engine::Engine(unsigned int width, unsigned int height)
	: _time(0.0f), _run(true)
{
	instance = this;
	Ogre::LogManager *log = new Ogre::LogManager();
	_log = Ogre::LogManager::getSingleton().createLog("OgreLogfile.log", true, true, false);
	_log->setDebugOutputEnabled(true);
	_root = new Ogre::Root();
	Ogre::RenderSystemList rsys = _root->getAvailableRenderers();
	for (Ogre::int32 i = 0; i < rsys.size(); i++)
	{
		if (rsys[i]->getName().find("OpenGL") != Ogre::String::npos)
			_root->setRenderSystem(rsys[i]);
	}
	_root->addFrameListener(this);
	_window = _root->initialise(true, "FP3D Alpha");
	_smgr = _root->createSceneManager(Ogre::ST_GENERIC);
}

Engine::~Engine()
{
	delete (_player);
	delete (_root);
}

bool Engine::init()
{
	if (!_root || !_window)
		return (false);
	initOIS();
	_player = new Player(_root, _smgr);
	_world = new World(_root, _smgr);
	tutorial();
	_gameState = Playing;
	return (true);
}

void Engine::initOIS()
{
	unsigned int width, height, bit;
	OIS::ParamList pl;
	size_t windowHnd = 0;
	std::ostringstream windowHndStr;

	_window->getCustomAttribute("WINDOW", &windowHnd);
	windowHndStr << windowHnd;
	pl.insert(std::make_pair(std::string("WINDOW"), windowHndStr.str()));
	_imgr = OIS::InputManager::createInputSystem(pl);
	_mouse = static_cast<OIS::Mouse*>(_imgr->createInputObject(OIS::OISMouse, false));
	_keyboard = static_cast<OIS::Keyboard*>(_imgr->createInputObject(OIS::OISKeyboard, false));
	_window->getMetrics(width, height, bit);
	const OIS::MouseState &mouse = _mouse->getMouseState();
	mouse.width = width;
	mouse.height = height;
}

void Engine::drop()
{
	_run = false;
}

void Engine::tutorial()
{
	_smgr->setAmbientLight(Ogre::ColourValue(255, 255, 255));
}

void Engine::toggleMouseHook()
{
	OIS::ParamList pl;
	
	#if defined OIS_WIN32_PLATFORM
	pl.insert(std::make_pair(std::string("w32_mouse"), std::string("DISCL_FOREGROUND")));
	pl.insert(std::make_pair(std::string("w32_mouse"), std::string("DISCL_NONEXCLUSIVE")));
	pl.insert(std::make_pair(std::string("w32_keyboard"), std::string("DISCL_FOREGROUND")));
	pl.insert(std::make_pair(std::string("w32_keyboard"), std::string("DISCL_NONEXCLUSIVE")));
#elif defined OIS_LINUX_PLATFORM
	pl.insert(std::make_pair(std::string("x11_mouse_grab"), std::string("false")));
	pl.insert(std::make_pair(std::string("x11_mouse_hide"), std::string("false")));
	pl.insert(std::make_pair(std::string("x11_keyboard_grab"), std::string("false")));
	pl.insert(std::make_pair(std::string("XAutoRepeatOn"), std::string("true")));
#endif
}

bool Engine::frameStarted(const Ogre::FrameEvent &e)
{
	if (_keyboard->isKeyDown(OIS::KC_ESCAPE))
	{
		_gameState = (_gameState == Playing ? Menu : Playing);
		toggleMouseHook();
	}
	if (_keyboard->isKeyDown(OIS::KC_F10))
		drop();
	if (_keyboard->isKeyDown(OIS::KC_F5))
		_player->takeDamage(20);
	//_player->update(_root, _smgr, _gui);
	return (true);
}

bool Engine::frameEnded(const Ogre::FrameEvent &e)
{
	if (!_run)
		return (false);
	_time += e.timeSinceLastFrame;
	if (_time > 100.0f)
	{
		return (false);
	}
	return (true);
}

int Engine::run()
{
	_root->startRendering();
	return (0);
}