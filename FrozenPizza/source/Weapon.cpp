#include "Weapon.hh"
#include "Engine.hh"

Weapon::Weapon(const std::string &id)
	: Item(id)
{
	_weaponData = getWeaponDataXML(id);
}

Weapon::~Weapon()
{

}

t_weaponData *Weapon::getWeaponDataXML(const std::string &weaponId)
{
	t_weaponData *weaponData = new t_weaponData();
	pugi::xml_document doc;
	pugi::xml_parse_result xml;
	std::ifstream ifs(WEAPON_XMLDATA_PATH(weaponId));

	if (!ifs || !(xml = doc.load(ifs)))
		return (NULL);
	if (pugi::xml_node weaponXML = doc.child("Weapon"))
	{
		weaponData->damage = atoi(weaponXML.child_value("damage"));
		weaponData->cooldown = atof(weaponXML.child_value("cooldown"));
		weaponData->twoHanded = atob(weaponXML.child_value("twoHanded"));
		if (pugi::xml_node firearmXML = weaponXML.child("Firearm"))
		{
			weaponData->fad = new t_firearmData();
			weaponData->fad->magSize = atoi(firearmXML.child_value("magSize"));
			weaponData->fad->range = atoi(firearmXML.child_value("range"));
		}
		else
		{
			weaponData->fad = NULL;
		}
		return (weaponData);
	}
}