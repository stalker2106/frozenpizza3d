# FROZENPIZZA 3D #

## Contributors ##
+ Stabax Corp.
+ Maxime 'stalker2106' Martens

## About ##
port of FrozenPizza on Irrlicht C++ 3D Engine

## Binary Releases ##

You may head over to [Project homepage](http://vault.maximemartens.fr/FrozenPizza3D/) to check the latest releases.

## License ##

This project is released under MIT License
Copyright 2017 Stabax Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## Dependencies ##
+ Irrlicht for Graphics/OpenGL layer
+ CEventManager for handling events (topic link)

### Getting started ###

** You must have Irrlicht Installed and compiled for your Visual Studio version. **

1. Clone repository
2. Open \*.sln with VS2013 or later
3. Compile & run
4. ???
5. Enjoy
